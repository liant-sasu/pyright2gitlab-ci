# Pyright to GitLab CI

This is a simple project to generate an image allowing to rapidly use pyright and pyright-to-gitlab-ci in Gitlab CI pipelines

## Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Docker](https://www.docker.com)
- [Pyright](https://github.com/microsoft/pyright)
- [Pyright-to-gitlab-ci](https://github.com/microsoft/pyright/blob/main/docs/ci-integration.md)

## Usage

To use this, just create a job in the `.gitlab-ci.yml` like this:

```yaml
type_check:
  stage: test
  allow_failure: true
  image: registry.gitlab.com/liant-sasu/pyright2gitlab-ci:node-current-alpine-latest
  before_script:
    - >
      python3 -m venv .venv
      && source .venv/bin/activate
    - pip install -r requirements.txt
  script:
   - pyright --outputjson src tests > report_raw.json
  after_script:
   - pyright-to-gitlab-ci --src report_raw.json --output codequality-pyright.json --base_path .
  artifacts:
    reports:
      codequality: codequality-pyright.json
    expire_in: 1 week
    when: always

```

Job will fail according to the configuration of pyright in your repository.

### Docker HUB

Project is available on Dockerhub [here](https://hub.docker.com/r/rlaures/pyright2gitlab-ci).

## Support

First get support on [stack_overflow](https://stackoverflow.com), you can tag
me [@rlaures](https://stackoverflow.com/users/3129859/rlaures) or recruit me or
my team.

## Need more images or signal a bug/flaw

Use the [issue tracking system of Gitlab](https://gitlab.com/liant-sasu/pyright2gitlab-ci/-/issues) please.
Follow the contribution code and please be cordial.

## Contributing

See [Contribution file](./CONTRIBUTING.md) to contribute (basically, create an issue
[here](https://gitlab.com/liant-sasu/docker-template/-/issues/new) and be clear).

## Authors and acknowledgment

Contributors are from [Liant](https://gitlab.com/liant-sasu) (at this moment).

## To support us

Don't hesitate to go see what we can do for you:
- Research or Innovation projects: https://liant.dev
- Any other services: https://liant.services

## License

[MIT](./LICENSE)
