ARG BASE_IMAGE_NAME
ARG BASE_IMAGE_TAG

FROM $BASE_IMAGE_NAME:$BASE_IMAGE_TAG

ARG BASE_IMAGE_TAG

# Install pyright and pyright-to-gitlab-ci
RUN npm i -g pyright pyright-to-gitlab-ci

# Install Python3 and python environment
RUN case $BASE_IMAGE_TAG in \
    *alpine) \
        apk update && apk add python3 py3-pip py3-virtualenv ;; \
    *debian) \
        apt-get update -qq \
        && apt-get install -qqyy python3 python3-pip python3-venv ;; \
    esac

# ENTRYPOINT use the same as node image
